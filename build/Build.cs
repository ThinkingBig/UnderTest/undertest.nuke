using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Nuke.Common;
using Nuke.Common.CI;
using Nuke.Common.CI.AppVeyor;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.NUnit;
using Nuke.Common.Utilities;
using Nuke.Common.Utilities.Collections;

using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.Git.GitTasks;
using static Nuke.Common.Tools.NUnit.NUnitTasks;

namespace UnderTest.Nuke
{
  [PublicAPI]
  [AppVeyor(
    AppVeyorImage.VisualStudio2019,
    AppVeyorImage.Ubuntu1804,
    AutoGenerate = false,
    SkipTags = true,
    InvokedTargets = new[] { nameof(Push) })]
  class Build : NukeBuild
  {
    public static int Main()
    {
      return Execute<Build>(x => x.Pack);
    }

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly string Configuration = IsLocalBuild ? "Debug" : "Release";

    [Parameter("Api Key for Nuget.org when pushing our nuget package")]
    readonly string NugetOrgApiKey;

    [Solution(@"src/UnderTest.Nuke.sln")]
    readonly Solution Solution;

    const string TargetNugetServer = "https://www.nuget.org/api/v2/package";

    AbsolutePath SourceDirectory => RootDirectory / "src";
    Project NukeProject => Solution.GetProject("UnderTest.Nuke");

    AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

    AbsolutePath TestDirectory => SourceDirectory / "UnderTest.Nuke.Tests";

    IEnumerable<(Project Project, string Framework)> PublishConfigurations =>
      from project in new[] { NukeProject }
      from framework in project.GetTargetFrameworks()
      select (project, framework);

    Target Clean => _ => _
      .Executes(() =>
      {
        SourceDirectory
          .GlobDirectories("*/bin", "*/obj")
          .ForEach(DeleteDirectory);

        EnsureCleanDirectory(ArtifactsDirectory);
      });

    Target Restore => _ => _
      .DependsOn(Clean)
      .Executes(() =>
      {
        DotNetRestore(s => s
          .SetProcessWorkingDirectory(SourceDirectory)
          .SetProjectFile(Solution));
      });

    Target Compile => _ => _
      .DependsOn(Restore)
      .Executes(() =>
      {
        DotNetBuild(_ => _
          .SetProjectFile(Solution)
          .SetNoRestore(InvokedTargets.Contains(Restore))
          .SetConfiguration(Configuration));

        DotNetPublish(_ => _
            .SetConfiguration(Configuration)
            .EnableNoBuild()
            .CombineWith(PublishConfigurations, (_, v) => _
              .SetProject(v.Project)
              .SetFramework(v.Framework)),
          10);
      });

    public Target UnitTests =>
      _ =>
        _.DependsOn(Compile)
          .Executes(() =>
          {
            var runner = ToolPathResolver.GetPackageExecutable("NUnit.ConsoleRunner", "nunit3-console.exe");
            var testAssemblies = GlobFiles(TestDirectory, $"**/bin/{Configuration}/**/*.Tests.dll").NotEmpty();

            NUnit3(s => s

              .AddInputFiles(testAssemblies)
              .SetProcessToolPath(runner)
              );
          });

    public Target Pack =>
      _ => _
        .DependsOn(UnitTests)
        .Produces(ArtifactsDirectory / "*.nupkg")
        .Requires(() => IsReleaseConfiguration())
        .Executes(() =>
          DotNetPack(_ => _
            .SetProject(Solution.GetProject("UnderTest.Nuke"))
            .SetNoBuild(InvokedTargets.Contains(Compile))
            .SetConfiguration(Configuration)
            .SetOutputDirectory(ArtifactsDirectory)));

    public Target Push =>
      _ =>
        _.DependsOn(Pack)
          .OnlyWhenDynamic(() => OnABranchWeWantToPushToNugetOrg())
          .Requires(() => NugetOrgApiKey)
          .Requires(() => GitHasCleanWorkingCopy())
          .Requires(() => IsReleaseConfiguration())
          .Executes(() =>
          {
            GlobFiles(ArtifactsDirectory, "*.nupkg")
              .ForEach(x =>
              {
                DotNetNuGetPush(_ => _
                  .SetSource(TargetNugetServer)
                  .SetApiKey(NugetOrgApiKey)
                  .SetTargetPath(x)
                  .SetSkipDuplicate(true));
              });
          });

    public bool IsReleaseConfiguration() => Configuration.EqualsOrdinalIgnoreCase("Release");

    // our branching strategy is stable for production, release* for releases and hotfix* for hotfix items.
    public bool OnABranchWeWantToPushToNugetOrg()
    {
      var branch = this.GetCurrentBranch().ToLowerInvariant();
      Logger.Log(LogLevel.Normal, $"Current branch {branch}");

      return branch == "stable"
             || branch.StartsWith("release")
             || branch.StartsWith("hotfix");
    }

    string GetCurrentBranch()
    {
      var environmentVariable = Environment.GetEnvironmentVariable("APPVEYOR_REPO_BRANCH");
      if (!string.IsNullOrEmpty(environmentVariable))
      {
        return environmentVariable;
      }

      return Git("rev-parse --abbrev-ref HEAD", null, outputFilter: null).Select(x => x.Text).Single();
    }
  }
}
