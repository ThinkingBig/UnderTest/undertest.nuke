﻿namespace UnderTest.Nuke
{
  public enum DotNetExecutionMode
  {
    DotNetCore = 1,
    DotNetFramework = 2
  }
}
