using System;
using System.Collections.Generic;
using System.IO;
using JetBrains.Annotations;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;

namespace UnderTest.Nuke
{
  [PublicAPI]
  [Serializable]
  public class UnderTestRunSuiteSettings : ToolSettings
  {
    public override string ProcessToolPath
    {
      get
      {
        if (ExecutionMode == DotNetExecutionMode.DotNetCore)
        {
          return DotNetTasks.DotNetPath;
        }

        return ProjectFile;
      }
    }

    public string Configuration { get; set; }

    public DotNetExecutionMode ExecutionMode { get; set; } = DotNetExecutionMode.DotNetCore;

    public override string ProcessWorkingDirectory => Path.GetDirectoryName(ProjectFile);

    public override Action<OutputType, string> ProcessCustomLogger => UnderTestTasks.UnderTestLogger;

    public IReadOnlyList<string> DoNotRunTheseTags => DoNotRunTheseTagsInternal.AsReadOnly();
    internal List<string> DoNotRunTheseTagsInternal { get; set; } = new List<string>();

    public bool NoBuild { get; set; } = true;

    public IReadOnlyList<string> OnlyRunTheseTags => OnlyRunTheseTagsInternal.AsReadOnly();
    public List<string> OnlyRunTheseTagsInternal { get; set; } = new List<string>();

    public string ProjectFile { get; set; }

    public string ReportOutputFolder { get; set; }

    public string ReportOutputFileName { get; set; }

    protected override Arguments ConfigureProcessArguments(Arguments arguments)
    {
       var customAppArgs = new Arguments();
       customAppArgs
         .Add("--only-run-these-tags {value}", OnlyRunTheseTags, separator: ',')
         .Add("--do-not-run-these-tags {value}", DoNotRunTheseTags, separator: ',');

       if (!string.IsNullOrWhiteSpace(ReportOutputFolder))
       {
         customAppArgs.Add("--report-output-folder {value}", ReportOutputFolder);
       }

       if (string.IsNullOrWhiteSpace(ReportOutputFileName))
       {
         customAppArgs.Add("--report-output-file-name {value}", ReportOutputFileName);
       }

       if (ExecutionMode == DotNetExecutionMode.DotNetCore)
       {
         arguments
           .Add("run")
           .Add("--project {value}", ProjectFile);

         if (NoBuild)
         {
           arguments.Add("--no-build");
         }

         if (!string.IsNullOrEmpty(Configuration))
         {
           arguments.Add($"--configuration {Configuration}");
         }

         arguments.Add("-- {value}", customAppArgs.ToString(), customValue: true);
       }
       else if (ExecutionMode == DotNetExecutionMode.DotNetFramework)
       {
         arguments.Add("{value}", customAppArgs.ToString(), customValue: true);
       }

       return base.ConfigureProcessArguments(arguments);
    }
  }
}
