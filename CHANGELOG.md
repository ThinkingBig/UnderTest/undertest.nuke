# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleased
### added
- added framework 472 support
### changed


## 0.7.0 - 2020-08-23
### added 
- add ability to set ReportOutputFileName and ReportOutputFolder command line arguments #59
- add `ExecutionMode` to `UnderTestRunSuiteSettings` #61
- adds `Configuration` and `NoBuild` to `UnderTestRunSuiteSettings` #63
- adds a sample project for basic usage #66 
### changed
- updates `UnderTestTasks.UnderTest` to `UnderTestRunSuite` to avoid namespace conflicts when `using static`
- updates to the latest version of Nuke.Common #65
### breaking changes
- update in execution means that what is passed as the project has changed. It is now the `csproj` file.  See the README for an example #63

## 0.6.0 - 2019-10-09
### added 
- add support for UnderTest #48
### changed
- removes SpecFlow based as a dependency #47

## 0.5.0 - 2019-04-12
### added 
### changed
- add `Glob` as a nuspec dependency 
- (potentially breaking) upgrade to Nuke.Common 0.18.0 
- fix issue where reports folder is deleted in `Clean` step (#41)
- now replaces TestRun.zip if it exists (#42)

## 0.4.0 - 2019-02-25
### added 
- step to compile feature files before compiling
- UnderTestRunnerTasks which allow the UnderTest steps to be run in other NukeBuilds
   * Steps to set this up are:
       * Add `IUnderTestBuildRunner` to your `NukeBuild` class
       * Add a call to `UnderTestRunnerTasks.AcceptanceTestRunner` (which can be statically used)
```
public virtual Target AcceptanceTests =>
      _ =>
        _.DependsOn(Compile)
          .Executes(() =>
          {
            AcceptanceTestRunner(UnderTestSettings, s => s);
          });
```
       * Configure `UnderTestTargetSettings` to taste.
### changed
- (breaking) Moved setting properties on UnderTestBuildRunner to UnderTestSettings
- (breaking) Updated four settings to just be the filename/folder vs the full path (with the new defaults in brackets):
    * NUnitTestResultPath is now NUnitTestResultFilename (`TestResults.xml`)
    * NUnitExecutionReportFilename (`ExecutionReport.html`)
    * PackagedReportFilename (`TestRun.zip`)
    * SpecFlowStepReportFilename (`StepDefinitionReport.html`)
    * FeatureFilesFolder (`Features`)

## [0.3.1] 2019-02-23
### added
### changed
- Corrected issue with `TestFilter` not being settable via the command line.

## [0.3.0] - 2019-02-10
### added
- Ability to Specify Tests to Run
- Copy screenshots to the reports folder
- Allow the pickles reports to ignore tags

### changed
- Updated the version of Nuke to 0.15.0

## [0.2.0] - 2019-01-15
### added
- Add Nuget.Core as a dependency
- Documented the expected project structure

### changed
- (breaking) Fixed spelling mistake in a few properties
- Reports now generate even if tests fail
